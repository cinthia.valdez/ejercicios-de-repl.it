# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a string:
# Print a value:
# print(s)
n = int(input())
d = {}
for i in range(n):
    first, second = input().split()
    d[first] = second
    d[second] = first
print(d[input()])